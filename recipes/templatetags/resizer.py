from django import template

register = template.Library()


def resize_to(ingredient, serving):
    if not serving:
        serving = 1
    print("ingredient:", ingredient)
    print("serving:", serving)
    return ingredient * float(serving)


register.filter(resize_to)
