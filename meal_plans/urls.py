from django.urls import path
from meal_plans.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
)

urlpatterns = [
    path("meal_plans/", MealPlanListView.as_view(), name="mealplans_list"),
    path(
        "meal_plans/<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
    path(
        "meal_plans/create/",
        MealPlanCreateView.as_view(),
        name="create_mealplan",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_plan_edit",
    ),
    # path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]
